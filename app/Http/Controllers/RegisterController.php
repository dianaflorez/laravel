<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Register;
use Session;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ItemCreateRequest;
use App\Http\Requests\ItemUpdateRequest;
use Illuminate\Support\Facades\Validator;
use DB;
use Input;
use Storage;

class RegisterController extends Controller
{
     // Create (Create) 
    
     public function create()
     {
         $register = Register::all();
         return view('admin.register.create', compact('register'));
     }
 
     // Process of Creating a Record 
     public function store(ItemCreateRequest $request)
     {
         // Instance to the Register model that calls the 'register' table of the Database
         $register = new Register; 
     
         // I receive all the form data from the view 'create.blade.php'
         $register->firstname = $request->firstname;
         $register->lastname = $request->lastname;
         $register->email = $request->email;
         
         $register->save();
     
         // I redirect to the main view with a message
         return redirect('admin/register')->with('message','Save !');
     }
 
     // View all
     public function index()
     {
         $register = Register::all();
         return view('admin.register.index', compact('register')); 
     }
 
     // Update
     public function updateregister($id)
     {
         $register = Register::find($id);
         return view('admin/register.update',['register'=>$register]);
     }
 
     public function update(ItemUpdateRequest $request, $id)
     {        
         //  receive all the data from the Update form
         $register = Register::find($id);
         $register->firstname = $request->firstname;
         $register->lastname = $request->lastname;
         $register->email = $request->email;
         $register->save();
     
         Session::flash('message', ' Successfully edited !');
         return Redirect::to('admin/register');
     }
 
     // Delete
     public function delete($id)
     {
         $register = Register::find($id);
 
         Register::destroy($id);
 
         Session::flash('message', 'Successfully deleted !');
         return Redirect::to('admin/register');
     }
}
