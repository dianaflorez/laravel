<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Register extends Model
{
    // Instance table
    protected $table = 'register';
    
    // I declare the fields that I will use from the table
    protected $fillable = ['firstname', 'lastname', 'email'];

}
