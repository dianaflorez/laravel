<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Dashboard
Route::get('admin/dashboard', 'Dashboard@index')->name('admin/dashboard');

// CRUD
/* Create */
Route::get('admin/register/create', 'RegisterController@create')->name('admin/register/create');
Route::put('admin/register/store', 'RegisterController@store')->name('admin/register/store');

/* Read */
Route::get('admin/register', 'RegisterController@index')->name('admin/register');

/* Update */
Route::get('admin/register/updateregister/{id}', 'RegisterController@updateregister')->name('admin/register/updateregister');
Route::put('admin/register/update/{id}', 'RegisterController@update')->name('admin/register/update');

/* Delete */
Route::put('admin/register/delete/{id}', 'RegisterController@delete')->name('admin/register/delete');