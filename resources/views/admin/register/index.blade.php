<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Nube Colectiva">
        <link rel="shortcut icon" href="http://nubecolectiva.com/favicon.ico" />

        <meta name="theme-color" content="#000000" />

        <title>Como crear un CRUD con Laravel 5.8 y Bootstrap 4 </title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/app.css') }}">      

    </head>

<body> 

    <div class="container mb-5">
        <div class="row">
            <div class="col-md-12">
              <h1 style="font-size: 28px;" class=" text-center">CRUD TEST</h1>

              <div class="header">
                <div class="container">
                    <div class="row">
                    <div class="col-md-5">
                        <!-- Logo -->
                        <div class="logo">
                            <h1><a href="{{ route('admin/dashboard') }}">Administrador</a></h1>
                        </div>
                    </div>
                    
                    <div class="col-md-2">
                        <div class="navbar navbar-inverse" role="banner">
                            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                                <ul class="nav navbar-nav">
                                <li><a href="{{ route('admin/dashboard') }}">Administrador</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

            <div class="page-content">
                <div class="row">
                
                    <div class="col-md-2">
                        <div class="sidebar content-box" style="display: block;">

                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="{{ route('admin/dashboard') }}"> Dashboard</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{ route('admin/register') }}"> Register</a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                
                    <div class="col-md-10">

                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin/dashboard') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Register</li>
                            </ol>
                        </nav>
                
                <div class="row">

                    <div class="col-md-12">

                        <div class="content-box-large">

                            <div class="panel-heading">
                                <div class="panel-title"><h2>Register</h2></div>             
                            </div>
                            
                            <div class="panel-body">

                                @if(Session::has('message'))
                                    <div class="alert alert-primary" role="alert">
                                    {{ Session::get('message') }}
                                    </div>
                                @endif

                                <a href="{{ route('admin/register/create') }}" class="btn btn-success mt-4 ml-3">  Add </a>
                                <br />
                                <br />
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($register as $reg)
                                        <tr>
                                            <td class="v-align-middle">{{$reg->firstname}}</td>
                                            <td class="v-align-middle">{{$reg->lastname}}</td>
                                            <td class="v-align-middle">{{$reg->email}}</td>
                                            <td class="v-align-middle">
                                
                                                <form action="{{ route('admin/register/delete',$reg->id) }}" method="POST" class="form-horizontal" role="form" onsubmit="return confirmDelete()">
                                
                                                    <input type="hidden" name="_method" value="PUT">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <a href="{{ route('admin/register/updateregister',$reg->id) }}" class="btn btn-primary">Edit</a>
                                
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                
                                                </form>
                                
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>          
<hr>
       
</div>

<footer class="text-muted mt-3 mb-3">
    <div align="center">
        Desarrollado por DF
    </div> 
</footer>

<!-- Bootstrap JS -->
<script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>


<script type="text/javascript">
 
  function confirmDelete()
  {
  var x = confirm("Are you sure that you want to delete?");
  if (x)
     return true;
  else
     return false;
  }
 
</script>

</body>
</html>