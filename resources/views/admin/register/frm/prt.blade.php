<div class="row">
	<div class="col-md-12">
		<section class="panel"> 
			<div class="panel-body">
 
				@if ( !empty ( $register->id) )
 
					<div class="form-group">
						<label for="firstname" class="negrita">First Name:</label> 
						<div>
							<input class="form-control" placeholder="first name" required="required" name="firstname" type="text" id="firstname" value="{{ $register->firstname }}"> 
						</div>
					</div>
 
					<div class="form-group">
						<label for="lastname" class="negrita">Last Name:</label> 
						<div>
							<input class="form-control" placeholder="Last name" name="lastname" type="text" id="lastname" value="{{ $register->lastname }}"> 
						</div>
					</div>
 
					<div class="form-group">
						<label for="email" class="negrita">Email:</label> 
						<div>
							<input class="form-control" placeholder="@" required="required" name="email" type="text" id="email" value="{{ $register->email }}"> 
						</div>
					</div>

 
					@else
 
					<div class="form-group">
						<label for="firstname" class="negrita">First Name:</label> 
						<div>
							<input class="form-control" placeholder="First Name" required="required" name="firstname" type="text" id="firstname"> 
						</div>
					</div>
 
					<div class="form-group">
						<label for="lastname" class="negrita">Last Name:</label> 
						<div>
							<input class="form-control" placeholder="Last name" name="lastname" type="text" id="lastname"> 
						</div>
					</div>
 
					<div class="form-group">
						<label for="email" class="negrita">Email:</label> 
						<div>
							<input class="form-control" placeholder="@" required="required" name="email" type="text" id="email"> 
						</div>
					</div>
 
                    <div class="form-group row">
                        <div class="col-md-6 col-md-6 offset-md-4">
                            {!! NoCaptcha::display() !!}
                        </div>
                    </div>
 
				@endif
 
				<button type="submit" class="btn btn-info">Save</button>
				<a href="{{ route('admin/register') }}" class="btn btn-warning">Cancel</a>
 
				<br>
				<br>
 
			</div>
		</section>
	</div>
</div>